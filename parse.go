package jsontape

import (
	"fmt"
	"io"
	"unicode/utf8"
)

type Parser struct {
	r io.ByteReader
	// held is the last unread rune, or -1 if there is none.
	held rune
}

func Parse(r io.ByteReader) *Parser {
	return &Parser{
		r:    r,
		held: -1,
	}
}

// Document parses a single JSON object, array, or primitive from its reader.
// The resulting tape's first element has KindDocument unless the reader is at
// EOF. There is no other element if only whitespace remains in the reader.
func (p *Parser) Document() ([]Elem, error) {
	ws, c, err := p.space()
	ws.Kind = KindDocument
	var v []Elem
	// TODO(branden): parse mode to discard trailing whitespace?
	if !isEmpty(ws) {
		v = []Elem{ws}
	}
	if err != nil {
		if err == io.EOF {
			return v, err
		}
		return nil, fmt.Errorf("couldn't read start of stream: %w", err)
	}
	p.unread(c)
	v, err = p.value(nil)
	if err != nil {
		return v, fmt.Errorf("error decoding JSON document: %w", err)
	}
	return v, nil
}

func (p *Parser) byte() (byte, error) {
	// TODO(zeph): track bytes, lines, documents?
	return p.r.ReadByte()
}

func (p *Parser) unread(c rune) {
	if p.held != -1 {
		panic("jsontape: double push")
	}
	p.held = c
}

// rune decodes a UTF-8 sequence. An invalid sequence results in an error.
func (p *Parser) rune() (rune, error) {
	if p.held != -1 {
		c := p.held
		p.held = -1
		return c, nil
	}
	b, err := p.byte()
	if err != nil {
		if err == io.EOF {
			return utf8.RuneError, io.EOF
		}
		// TODO(zeph): syntax error types
		return utf8.RuneError, fmt.Errorf("couldn't read UTF-8 start: %w", err)
	}
	if b < 0x80 {
		// One-byte rune.
		return rune(b), nil
	}
	if !utf8.RuneStart(b) {
		return utf8.RuneError, fmt.Errorf("invalid UTF-8 start %#02x", b)
	}
	// We know b is a valid initial byte of a multibyte sequence. So, its two
	// most significant bits are 1, and the following two bits determine the
	// length of the sequence.
	// TODO(branden): this could probably be done much more efficiently
	counts := [4]uint8{0b00: 2, 0b01: 2, 0b10: 3, 0b11: 4}
	nb := int(counts[b>>4&0b11])
	v := []byte{0: b, utf8.UTFMax - 1: 0}[:nb]
	for i := 1; i < nb; i++ {
		b, err := p.byte()
		if err != nil {
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			return utf8.RuneError, fmt.Errorf("error decoding UTF-8 sequence: %w", err)
		}
		v[i] = b
	}
	r, n := utf8.DecodeRune(v)
	if n != len(v) {
		err = fmt.Errorf("invalid UTF-8 sequence %02x", v)
	}
	return r, err
}

// space consumes whitespace in the token stream. The returned token always has
// KindSpace and has empty content iff the whitespace can be encoded as 255 or
// fewer of each of newlines, tabs, and spaces, in that order. The rune result
// is the first non-whitespace character found, or [utf8.RuneError] if the
// input is exhausted or another error occurred. The rune is not unread.
//
// If the returned error is io.EOF, the element may be valid. The caller should
// check that the element is empty.
func (p *Parser) space() (Elem, rune, error) {
	// TODO(zeph): parse mode to skip whitespace?
	e := Elem{Kind: KindSpace}
	for {
		c, err := p.rune()
		if err != nil {
			return e, utf8.RuneError, err
		}
		switch c {
		case '\n':
			if e.Lines == 255 || e.Tabs > 0 || e.Spaces > 0 {
				p.unread(c)
				return p.fullws(e.Lines, e.Tabs, e.Spaces)
			}
			e.Lines++
		case '\t':
			if e.Tabs == 255 || e.Spaces > 0 {
				p.unread(c)
				return p.fullws(e.Lines, e.Tabs, e.Spaces)
			}
			e.Tabs++
		case ' ':
			if e.Spaces == 255 {
				p.unread(c)
				return p.fullws(e.Lines, e.Tabs, e.Spaces)
			}
			e.Spaces++
		case '\r':
			p.unread(c)
			return p.fullws(e.Lines, e.Tabs, e.Spaces)
		default:
			return e, c, nil
		}
	}
}

// fullws is the fallback path of p.space for space that cannot be encoded
// using the element fields.
func (p *Parser) fullws(lines, tabs, spaces uint8) (Elem, rune, error) {
	b := make([]byte, int(lines)+int(tabs)+int(spaces))
	// Fill in the whitespace we already have.
	{
		i := copy(b, _newlines[:lines])
		i += copy(b[i:], _tabs[:tabs])
		copy(b[i:], _spaces[:spaces])
	}
	for {
		c, err := p.rune()
		if err != nil {
			e := Elem{
				Content: string(b),
				Kind:    KindSpace,
			}
			return e, utf8.RuneError, err
		}
		switch c {
		case ' ', '\n', '\t', '\r':
			b = append(b, byte(c))
		default:
			e := Elem{
				Content: string(b),
				Kind:    KindSpace,
			}
			return e, c, nil
		}
	}
}

// terminates returns true if c terminates a token, i.e. if c is a JSON
// whitespace character, comma, or close delimiter.
func terminates(c rune) bool {
	switch c {
	case ' ', '\t', '\n', '\r', ',', ']', '}':
		return true
	default:
		return false
	}
}

// literal recognizes a literal value with the given text. The byte stream must
// match bytewise and must be followed by whitespace, a comma or close
// delimiter, or EOF. The recognized text is consumed and discarded.
func (p *Parser) literal(s string) error {
	for _, r := range s {
		c, err := p.rune()
		if err != nil {
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			return err
		}
		if c != r {
			// TODO(zeph): consume to end of token
			// TODO(zeph): syntax error types
			if terminates(c) {
				return fmt.Errorf("literal terminated too soon")
			}
			return fmt.Errorf("unexpected %[1]U %[1]q in literal", c)
		}
	}
	// Check for end of token.
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			return nil
		}
	}
	if !terminates(c) {
		// TODO(zeph): consume to end of token
		return fmt.Errorf("literal unterminated, found %[1]U %[1]q", c)
	}
	p.unread(c)
	return nil
}

// null recognizes a null given that the initial n is consumed.
func (p *Parser) null() error {
	if err := p.literal("ull"); err != nil {
		return fmt.Errorf("bad null literal: %w", err)
	}
	return nil
}

// false recognizes a false given that the initial f is consumed.
func (p *Parser) false() error {
	if err := p.literal("alse"); err != nil {
		return fmt.Errorf("bad false literal: %w", err)
	}
	return nil
}

// true recognizes a true given that the initial t is consumed.
func (p *Parser) true() error {
	if err := p.literal("rue"); err != nil {
		return fmt.Errorf("bad true literal: %w", err)
	}
	return nil
}

// number parses a JSON number.
func (p *Parser) number() (Elem, error) {
	e := Elem{Kind: KindNum}
	// Most numbers will be smaller than 32 bytes. Preallocate.
	b := make([]byte, 0, 32)
	// Check for -.
	c, err := p.rune()
	if err != nil {
		// Technically unreachable in the normal parsing flow since this
		// must be an unread rune, but for testing, don't assume.
		return e, fmt.Errorf("couldn't check for negative sign in number: %w", err)
	}
	if c != '-' {
		p.unread(c)
	} else {
		b = append(b, '-')
	}
	// Scan parts.
	b, err = p.digits(b)
	if err != nil {
		// TODO(zeph): advance to terminator
		e.Content = string(b)
		e.Kind = KindErr
		return e, fmt.Errorf("couldn't get digits of integer part: %w", err)
	}
	b, err = p.frac(b)
	if err != nil {
		e.Content = string(b)
		e.Kind = KindErr
		return e, err
	}
	b, err = p.exponent(b)
	if err != nil {
		e.Content = string(b)
		e.Kind = KindErr
		return e, err
	}
	// TODO(zeph): check that next character is a terminator
	e.Content = string(b)
	return e, nil
}

// digits appends the following sequence of digits to b.
func (p *Parser) digits(b []byte) ([]byte, error) {
	// Do one fixed iteration for error checking.
	// Nowhere can we have an empty digit sequence.
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
		return b, err
	}
	if c < '0' || c > '9' {
		return b, fmt.Errorf("empty digit sequence, found %[1]U %[1]q", c)
	}
	b = append(b, byte(c))

	for {
		c, err := p.rune()
		if err != nil {
			if err == io.EOF {
				err = nil
			}
			return b, err
		}
		if '0' <= c && c <= '9' {
			b = append(b, byte(c))
			continue
		}
		p.unread(c)
		return b, nil
	}
}

// frac parses the fractional part of a number if it exists and appends it to b.
func (p *Parser) frac(b []byte) ([]byte, error) {
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			return b, nil
		}
		return b, fmt.Errorf("couldn't check for fractional part: %w", err)
	}
	if c != '.' {
		p.unread(c)
		return b, nil
	}
	b = append(b, '.')
	b, err = p.digits(b)
	if err != nil {
		return b, fmt.Errorf("couldn't get digits of fractional part: %w", err)
	}
	return b, nil
}

// exponent parses the exponent part of a number if it exists and appends it
// to b.
func (p *Parser) exponent(b []byte) ([]byte, error) {
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			return b, nil
		}
		return b, fmt.Errorf("couldn't check for exponent in number: %w", err)
	}
	if c != 'e' && c != 'E' {
		// No exponent.
		p.unread(c)
		return b, nil
	}
	b = append(b, byte(c))
	// Check for sign.
	c, err = p.rune()
	if err != nil {
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
		return b, fmt.Errorf("couldn't check for exponent sign: %w", err)
	}
	if c == '-' || c == '+' {
		b = append(b, byte(c))
	} else {
		p.unread(c)
	}
	b, err = p.digits(b)
	if err != nil {
		return b, fmt.Errorf("couldn't get exponent digits: %w", err)
	}
	return b, nil
}

// string parses a string assuming the initial " has been consumed.
func (p *Parser) string() (Elem, error) {
	e := Elem{Kind: KindString}
	// Assume most strings are short and preallocate.
	b := make([]byte, 1, 32)
	b[0] = '"'
	for {
		c, err := p.rune()
		if err != nil {
			if err == io.EOF {
				e.Kind = KindErr
				e.Content = string(b)
				return e, fmt.Errorf("unterminated string: %w", io.ErrUnexpectedEOF)
			}
			return e, fmt.Errorf("couldn't parse string: %w", err)
		}
		b = utf8.AppendRune(b, c)
		switch c {
		case '"':
			e.Content = string(b)
			return e, nil
		case '\\':
			b, err = p.escape(b)
			if err != nil {
				e.Kind = KindErr
				e.Content = string(b)
				return e, err
			}
		case 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31:
			// U+0000 through U+001F MUST be escaped.
			// TODO(zeph): lax parse mode?
			e.Kind = KindErr
			e.Content = string(b)
			return e, fmt.Errorf("illegal control code in string: %q", c)
		}
	}
}

// escape parses a string escape sequence assuming the initial \ has been
// consumed and appends it onto b. The caller should append the \ to b.
func (p *Parser) escape(b []byte) ([]byte, error) {
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			err = io.ErrUnexpectedEOF
		}
		return b, fmt.Errorf("couldn't get escaped value: %w", err)
	}
	b = utf8.AppendRune(b, c)
	switch c {
	case '"', '\\', '/', 'b', 'f', 'n', 'r', 't':
		// Single-character escape sequences.
		return b, nil
	case 'u':
		// Five-character Unicode escape.
		b, err = p.unicode(b)
		return b, err
	default:
		// TODO(zeph): KindErr
		// TODO(zeph): consume to end of string
		return b, fmt.Errorf("illegal escape character %q", c)
	}
}

// unicode parses the digits of a Unicode escape sequence.
func (p *Parser) unicode(b []byte) ([]byte, error) {
	for i := 0; i < 4; i++ {
		c, err := p.rune()
		if err != nil {
			if err == io.EOF {
				err = io.ErrUnexpectedEOF
			}
			return b, fmt.Errorf("couldn't read Unicode escape sequence: %w", err)
		}
		switch c {
		case '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
			'a', 'b', 'c', 'd', 'e', 'f',
			'A', 'B', 'C', 'D', 'E', 'F':
			b = append(b, byte(c))
		case '"':
			// Unread so that we finish out the string properly.
			p.unread(c)
			// TODO(zeph): KindErr
			return b, fmt.Errorf("invalid Unicode escape sequence: have %d of 4 digits", i+1)
		default:
			b = utf8.AppendRune(b, c)
			// TODO(zeph): continue reading escape sequence?
			return b, fmt.Errorf("invalid Unicode escape sequence: %[1]U %[1]q is not a hex digit", c)
		}
	}
	return b, nil
}

// array parses an array assuming the initial [ has been consumed and is
// provided in e. The array contents up to and not including the ] element are
// appended to v. The ] is returned. In the event of a parse error, the
// returned element may instead be a KindErr with empty content.
func (p *Parser) array(v []Elem, e Elem) ([]Elem, Elem, error) {
	leftk := len(v)

	// A couple helper functions to reduce line count.
	fail := func(err error, msg string, e Elem) ([]Elem, Elem, error) {
		if leftk < len(v) {
			v[leftk].Kind = KindErr
		}
		if err == io.EOF {
			return v, e, fmt.Errorf("unterminated array: %w", io.ErrUnexpectedEOF)
		}
		return v, e, fmt.Errorf("couldn't %s: %w", msg, err)
	}
	mkpair := func() int {
		if leftk < len(v) {
			v[leftk].Neighbor = len(v) - leftk
		}
		return leftk - len(v)
	}

	ws, c, err := p.space()
	v = copySpace(v, e, ws)
	if err != nil {
		return fail(err, "parse array", Elem{Kind: KindErr})
	}
	switch c {
	case ']':
		// Empty array.
		r := Elem{
			Content:  "]",
			Neighbor: mkpair(),
			Kind:     KindListEnd,
		}
		return v, r, nil
	case '}':
		r := Elem{
			Content:  "}",
			Neighbor: mkpair(),
			Kind:     KindErr,
		}
		return fail(fmt.Errorf("mismatched brackets: [}"), "parse array", r)
	case ',':
		r := Elem{
			Content: ",",
			Kind:    KindErr,
		}
		// TODO(zeph): parse to closing bracket
		return v, r, fmt.Errorf("missing array element")
	}
	p.unread(c)
	// Read the first value outside the loop to enforce comma rules.
	// TODO(zeph): lax parse mode for [x,]
	v, err = p.value(v)
	if err != nil {
		return fail(err, "parse array element", Elem{Kind: KindErr})
	}
	c, err = p.rune()
	if err != nil {
		return fail(err, "parse array", Elem{Kind: KindErr})
	}
	switch c {
	case ']':
		// Singleton array.
		r := Elem{
			Content:  "]",
			Neighbor: mkpair(),
			Kind:     KindListEnd,
		}
		return v, r, nil
	case ',':
		ws, c, err := p.space()
		v = copySpace(v, Elem{Content: ",", Kind: KindComma}, ws)
		if err != nil {
			return fail(err, "parse array", Elem{Kind: KindErr})
		}
		p.unread(c)
	default:
		return fail(fmt.Errorf("unexpected character %[1]U %[1]q following array element", c), "parse array", Elem{Kind: KindErr})
	}
	// Now we're at the second value in the array and can proceed with the loop.
	for {
		v, err = p.value(v)
		if err != nil {
			// TODO(zeph): this approach means a trailing comma in the list
			// can't match brackets
			return fail(err, "parse array element", Elem{Kind: KindErr})
		}
		c, err = p.rune()
		if err != nil {
			return fail(err, "parse array", Elem{Kind: KindErr})
		}
		switch c {
		case ']':
			r := Elem{
				Content:  "]",
				Neighbor: mkpair(),
				Kind:     KindListEnd,
			}
			return v, r, nil
		case ',':
			ws, c, err := p.space()
			v = copySpace(v, Elem{Content: ",", Kind: KindComma}, ws)
			if err != nil {
				return fail(err, "parse array", Elem{Kind: KindErr})
			}
			p.unread(c)
		default:
			return fail(fmt.Errorf("unexpected character %[1]U %[1]q following array element", c), "parse array", Elem{Kind: KindErr})
		}
	}
}

// value parses a complete JSON value and appends its token stream to v.
func (p *Parser) value(v []Elem) ([]Elem, error) {
	var e Elem
	c, err := p.rune()
	if err != nil {
		if err == io.EOF {
			return v, err
		}
		return v, fmt.Errorf("couldn't determine value type: %w", err)
	}
	switch c {
	case 'n':
		if err := p.null(); err != nil {
			return v, err
		}
		e.Content = "null"
		e.Kind = KindNull
	case 'f':
		if err := p.false(); err != nil {
			return v, err
		}
		e.Content = "false"
		e.Kind = KindFalse
	case 't':
		if err := p.true(); err != nil {
			return v, err
		}
		e.Content = "true"
		e.Kind = KindTrue
	case '-', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9':
		p.unread(c)
		e, err = p.number()
		if err != nil {
			return v, err
		}
	case '"':
		e, err = p.string()
		if err != nil {
			return v, err
		}
	case '[':
		e = Elem{
			Content: "[",
			Kind:    KindListStart,
		}
		v, e, err = p.array(v, e)
		if err != nil {
			return v, err
		}
	case '{':
		// TODO(zeph): object
	case ']', '}':
		e = Elem{
			Content: string(c),
			Kind:    KindErr,
		}
		prev := c
		// Consume following space so we don't end up stuck on the next call.
		// TODO(zeph): better to just not return on syntax error
		ws, c, err := p.space()
		v = copySpace(v, e, ws)
		if err != nil {
			if err == io.EOF {
				return v, fmt.Errorf("mismatched bracket: no opening for %c", prev)
			}
			return v, fmt.Errorf("couldn't scan whitespace after unopened %c bracket: %w", prev, err)
		}
		p.unread(c)
		return v, fmt.Errorf("mismatched bracket: no opening for %c", prev)
	default:
		e = Elem{
			Content: string(c),
			Kind:    KindErr,
		}
		ws, c, err := p.space()
		v = copySpace(v, e, ws)
		if err != nil {
			if err == io.EOF {
				return v, fmt.Errorf("invalid syntax: %[1]U %[1]q cannot start an expression", c)
			}
			return v, fmt.Errorf("couldn't scan whitespace after invalid %[1]U %[1]q: %[2]w", c, err)
		}
		p.unread(c)
		return v, fmt.Errorf("invalid syntax: %[1]U %[1]q cannot start an expression", c)
	}
	ws, c, err := p.space()
	v = copySpace(v, e, ws)
	if err != nil {
		if err == io.EOF {
			// Wait til next time.
			err = nil
		}
		return v, err
	}
	p.unread(c)
	return v, nil
}

func isEmpty(e Elem) bool {
	return e.Content == "" && e.Lines == 0 && e.Tabs == 0 && e.Spaces == 0
}

// copySpace appends a syntactic equivalent combination of e and ws to the
// tape. e must have empty whitespace counters.
func copySpace(v []Elem, e, ws Elem) []Elem {
	if ws.Content == "" {
		e.Lines, e.Tabs, e.Spaces = ws.Lines, ws.Tabs, ws.Spaces
		return append(v, e)
	}
	return append(v, e, ws)
}

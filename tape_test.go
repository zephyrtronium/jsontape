/*
   Copyright 2023 Branden J Brown

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package jsontape

import (
	"bytes"
	"testing"
)

func TestPrewrittenArrays(t *testing.T) {
	cases := []struct {
		name string
		arr  [256]byte
		char byte
	}{
		{name: "_newlines", arr: _newlines, char: '\n'},
		{name: "_tabs", arr: _tabs, char: '\t'},
		{name: "_spaces", arr: _spaces, char: ' '},
	}
	for _, c := range cases {
		for k, v := range c.arr {
			if v != c.char {
				t.Errorf("elem %d of %s is not %q", k, c.name, c.char)
			}
		}
	}
}

func TestElemString(t *testing.T) {
	cases := []struct {
		name string
		elem Elem
		want string
	}{
		{
			name: "space",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindSpace,
			},
			want: "",
		},
		{
			name: "document",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindDocument,
			},
			want: "",
		},
		{
			name: "string",
			elem: Elem{
				Content: `"\bocchi"`,
				Kind:    KindString,
			},
			want: `"\bocchi"`,
		},
		{
			name: "newlines",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Lines:   2,
			},
			want: "bocchi",
		},
		{
			name: "tabs",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Tabs:    2,
			},
			want: "bocchi",
		},
		{
			name: "spaces",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Spaces:  2,
			},
			want: "bocchi",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			got := c.elem.String()
			if got != c.want {
				t.Errorf("wrong string: want %q, got %q", c.want, got)
			}
		})
	}
}

func TestElemWriteTo(t *testing.T) {
	cases := []struct {
		name string
		elem Elem
		want string
	}{
		{
			name: "space",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindSpace,
			},
			want: "bocchi",
		},
		{
			name: "document",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindDocument,
			},
			want: "bocchi",
		},
		{
			name: "string",
			elem: Elem{
				Content: `"\bocchi"`,
				Kind:    KindString,
			},
			want: `"\bocchi"`,
		},
		{
			name: "newlines",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Lines:   2,
			},
			want: "bocchi\n\n",
		},
		{
			name: "tabs",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Tabs:    2,
			},
			want: "bocchi\t\t",
		},
		{
			name: "spaces",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Spaces:  2,
			},
			want: "bocchi  ",
		},
		{
			name: "whitespace",
			elem: Elem{
				Content: "bocchi",
				Kind:    KindErr,
				Lines:   2,
				Tabs:    2,
				Spaces:  2,
			},
			want: "bocchi\n\n\t\t  ",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			var (
				b   bytes.Buffer
				n   int64
				err error
			)
			alloc := testing.AllocsPerRun(100, func() {
				b.Reset()
				n, err = c.elem.WriteTo(&b)
			})
			if err != nil {
				t.Errorf("unexpected error: %v", err)
			}
			if int(n) != len(c.want) {
				t.Errorf("wrong number of bytes written: want %d, got %d", len(c.want), n)
			}
			if b.String() != c.want {
				t.Errorf("wrong result: want %q, got %q", b.String(), c.want)
			}
			if alloc != 0 {
				t.Errorf("too many allocations: want 0, got %v", alloc)
			}
		})
	}
}

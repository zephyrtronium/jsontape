package jsontape

import (
	"bytes"
	"fmt"
	"io"
	"slices"
	"strings"
	"testing"
	"unicode/utf8"
)

func TestParseRune(t *testing.T) {
	cases := []struct {
		name  string
		bytes []byte
		want  rune
		err   string
	}{
		{
			name:  "empty",
			bytes: nil,
			want:  utf8.RuneError,
			err:   "EOF",
		},
		{
			name:  "ascii",
			bytes: []byte("b"),
			want:  'b',
		},
		{
			name:  "ascii-multi",
			bytes: []byte("bocchi"),
			want:  'b',
		},
		{
			name:  "2",
			bytes: []byte("µ"),
			want:  'µ',
		},
		{
			name:  "2-multi",
			bytes: []byte("µµ"),
			want:  'µ',
		},
		{
			name:  "3",
			bytes: []byte("€"),
			want:  '€',
		},
		{
			name:  "3-multi",
			bytes: []byte("€€"),
			want:  '€',
		},
		{
			name:  "4",
			bytes: []byte("🐢"),
			want:  '🐢',
		},
		{
			name:  "4-multi",
			bytes: []byte("🐢🐢"),
			want:  '🐢',
		},
		{
			name:  "bad-start",
			bytes: []byte{0x80},
			want:  utf8.RuneError,
			err:   "start",
		},
		{
			name:  "short",
			bytes: []byte{0xc2},
			want:  utf8.RuneError,
			err:   "unexpected EOF",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(bytes.NewReader(c.bytes))
			got, err := p.rune()
			if got != c.want {
				t.Errorf("wrong decoded rune: want %U, got %U", c.want, got)
			}
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

func TestParseLiteral(t *testing.T) {
	cases := []struct {
		name  string
		src   string
		check string
		err   string
	}{
		{
			name:  "ok",
			src:   "ull",
			check: "ull",
			err:   "",
		},
		{
			name:  "short",
			src:   "ul",
			check: "ull",
			err:   "unexpected EOF",
		},
		{
			name:  "short-terminated",
			src:   "ul ",
			check: "ull",
			err:   "terminated",
		},
		{
			name:  "long",
			src:   "ulll",
			check: "ull",
			err:   "unterminated",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			err := p.literal(c.check)
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

func TestParseSpace(t *testing.T) {
	cases := []struct {
		name string
		src  string
		want Elem
		term rune
		eof  bool
	}{
		{
			name: "empty",
			src:  "",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    0,
				Spaces:  0,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "newline",
			src:  "\n\n",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   2,
				Tabs:    0,
				Spaces:  0,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "tabs",
			src:  "\t\t",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    2,
				Spaces:  0,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "spaces",
			src:  "  ",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    0,
				Spaces:  2,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "cr",
			src:  "\r\r",
			want: Elem{
				Content: "\r\r",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    0,
				Spaces:  0,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "unidiomatic",
			src:  " \t",
			want: Elem{
				Content: " \t",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    0,
				Spaces:  0,
			},
			term: utf8.RuneError,
			eof:  true,
		},
		{
			name: "terminated",
			src:  "\nnull",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   1,
				Tabs:    0,
				Spaces:  0,
			},
			term: 'n',
			eof:  false,
		},
		{
			name: "none",
			src:  "null",
			want: Elem{
				Content: "",
				Kind:    KindSpace,
				Lines:   0,
				Tabs:    0,
				Spaces:  0,
			},
			term: 'n',
			eof:  false,
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			got, r, err := p.space()
			if err != nil && err != io.EOF {
				t.Errorf("unexpected error: %v", err)
			}
			if got != c.want {
				t.Errorf("wrong result: want %#v, got %#v", c.want, got)
			}
			if r != c.term {
				t.Errorf("wrong terminating rune: want %[1]U %[1]q, got %[2]U %[2]q", c.term, r)
			}
			if (err == io.EOF) != c.eof {
				t.Errorf("wrong eofness: want %t, got %t", c.eof, err == io.EOF)
			}
		})
	}
}

func TestParseNumber(t *testing.T) {
	cases := []struct {
		name string
		src  string
		want Elem
		err  string
	}{
		{
			name: "zero",
			src:  "0",
			want: Elem{
				Content: "0",
				Kind:    KindNum,
			},
		},
		{
			name: "zero-terminated",
			src:  "0 ",
			want: Elem{
				Content: "0",
				Kind:    KindNum,
			},
		},
		{
			name: "minus-zero",
			src:  "-0",
			want: Elem{
				Content: "-0",
				Kind:    KindNum,
			},
		},
		{
			name: "frac",
			src:  "0.0",
			want: Elem{
				Content: "0.0",
				Kind:    KindNum,
			},
		},
		{
			name: "exp",
			src:  "0.0e0",
			want: Elem{
				Content: "0.0e0",
				Kind:    KindNum,
			},
		},
		{
			name: "exp-minus",
			src:  "0.0e-0",
			want: Elem{
				Content: "0.0e-0",
				Kind:    KindNum,
			},
		},
		{
			name: "exp-plus",
			src:  "0.0e+0",
			want: Elem{
				Content: "0.0e+0",
				Kind:    KindNum,
			},
		},
		{
			name: "no-digits",
			src:  "- ",
			want: Elem{
				Content: "-",
				Kind:    KindErr,
			},
			err: "integer",
		},
		{
			name: "no-frac-digits",
			src:  "0. ",
			want: Elem{
				Content: "0.",
				Kind:    KindErr,
			},
			err: "fractional",
		},
		{
			name: "no-exp-digits",
			src:  "0.0e ",
			want: Elem{
				Content: "0.0e",
				Kind:    KindErr,
			},
			err: "exponent",
		},
		{
			name: "no-exp-digits-sign",
			src:  "0.0e- ",
			want: Elem{
				Content: "0.0e-",
				Kind:    KindErr,
			},
			err: "exponent",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			got, err := p.number()
			if got != c.want {
				t.Errorf("wrong elem: want %#v, got %#v", c.want, got)
			}
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

func TestParseString(t *testing.T) {
	cases := []struct {
		name string
		src  string
		want Elem
		err  string
	}{
		{
			name: "empty",
			src:  `"`,
			want: Elem{
				Content: `""`,
				Kind:    KindString,
			},
		},
		{
			name: "text",
			src:  `bocchi"`,
			want: Elem{
				Content: `"bocchi"`,
				Kind:    KindString,
			},
		},
		{
			name: "terminated",
			src:  `bocchi" `,
			want: Elem{
				Content: `"bocchi"`,
				Kind:    KindString,
			},
		},
		{
			name: "escape",
			src:  `\"\\\/\b\f\n\r\t\u005C\ud834\udd1e"`,
			want: Elem{
				Content: `"\"\\\/\b\f\n\r\t\u005C\ud834\udd1e"`,
				Kind:    KindString,
			},
		},
		{
			name: "long-unicode",
			src:  `\u12345"`,
			want: Elem{
				Content: `"\u12345"`,
				Kind:    KindString,
			},
		},
		{
			name: "unterminated",
			src:  `bocchi`,
			want: Elem{
				Content: `"bocchi`,
				Kind:    KindErr,
			},
			err: "unterminated",
		},
		{
			name: "control",
			src:  "\n\"",
			want: Elem{
				Content: "\"\n",
				Kind:    KindErr,
			},
			err: "control",
		},
		{
			name: "bad-escape",
			src:  `\z"`,
			want: Elem{
				Content: `"\z`,
				Kind:    KindErr,
			},
			err: "escape",
		},
		{
			name: "short-unicode",
			src:  `\u000"`,
			want: Elem{
				Content: `"\u000`,
				Kind:    KindErr,
			},
			err: "Unicode",
		},
		{
			name: "bad-unicode",
			src:  `\u000x"`,
			want: Elem{
				Content: `"\u000x`,
				Kind:    KindErr,
			},
			err: fmt.Sprintf("%U", 'x'),
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			got, err := p.string()
			if got != c.want {
				t.Errorf("wrong elem: want %#v, got %#v", c.want, got)
			}
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

func TestParseArray(t *testing.T) {
	cases := []struct {
		name string
		src  string
		want []Elem
		err  string
	}{
		{
			name: "empty",
			src:  `]`,
			want: []Elem{
				{Content: "[", Neighbor: 1, Kind: KindListStart},
				{Content: "]", Neighbor: -1, Kind: KindListEnd},
			},
		},
		{
			name: "empty-space",
			src:  ` ]`,
			want: []Elem{
				{Content: "[", Neighbor: 1, Kind: KindListStart, Spaces: 1},
				{Content: "]", Neighbor: -1, Kind: KindListEnd},
			},
		},
		{
			name: "singleton",
			src:  `null]`,
			want: []Elem{
				{Content: "[", Neighbor: 2, Kind: KindListStart},
				{Content: "null", Neighbor: 0, Kind: KindNull},
				{Content: "]", Neighbor: -2, Kind: KindListEnd},
			},
		},
		{
			name: "singleton-space",
			src:  ` null ]`,
			want: []Elem{
				{Content: "[", Neighbor: 2, Kind: KindListStart, Spaces: 1},
				{Content: "null", Neighbor: 0, Kind: KindNull, Spaces: 1},
				{Content: "]", Neighbor: -2, Kind: KindListEnd},
			},
		},
		{
			name: "triple",
			src:  `null,false,true]`,
			want: []Elem{
				{Content: "[", Neighbor: 6, Kind: KindListStart},
				{Content: "null", Neighbor: 0, Kind: KindNull},
				{Content: ",", Neighbor: 0, Kind: KindComma},
				{Content: "false", Neighbor: 0, Kind: KindFalse},
				{Content: ",", Neighbor: 0, Kind: KindComma},
				{Content: "true", Neighbor: 0, Kind: KindTrue},
				{Content: "]", Neighbor: -6, Kind: KindListEnd},
			},
		},
		{
			name: "triple-space",
			src:  ` null , false , true ]`,
			want: []Elem{
				{Content: "[", Neighbor: 6, Kind: KindListStart, Spaces: 1},
				{Content: "null", Neighbor: 0, Kind: KindNull, Spaces: 1},
				{Content: ",", Neighbor: 0, Kind: KindComma, Spaces: 1},
				{Content: "false", Neighbor: 0, Kind: KindFalse, Spaces: 1},
				{Content: ",", Neighbor: 0, Kind: KindComma, Spaces: 1},
				{Content: "true", Neighbor: 0, Kind: KindTrue, Spaces: 1},
				{Content: "]", Neighbor: -6, Kind: KindListEnd},
			},
		},
		{
			name: "nested",
			src:  `[[]]]`,
			want: []Elem{
				{Content: "[", Neighbor: 5, Kind: KindListStart},
				{Content: "[", Neighbor: 3, Kind: KindListStart},
				{Content: "[", Neighbor: 1, Kind: KindListStart},
				{Content: "]", Neighbor: -1, Kind: KindListEnd},
				{Content: "]", Neighbor: -3, Kind: KindListEnd},
				{Content: "]", Neighbor: -5, Kind: KindListEnd},
			},
		},
		{
			name: "unterminated",
			src:  ``,
			want: []Elem{
				{Content: "[", Neighbor: 0, Kind: KindErr},
				{Kind: KindErr},
			},
			err: "unterminated",
		},
		{
			name: "mismatched",
			src:  `}`,
			want: []Elem{
				{Content: "[", Neighbor: 1, Kind: KindErr},
				{Content: "}", Neighbor: -1, Kind: KindErr},
			},
			err: "mismatched",
		},
		{
			name: "missing-first",
			src:  `,null]`,
			want: []Elem{
				{Content: "[", Neighbor: 0, Kind: KindListStart},
				{Content: ",", Neighbor: 0, Kind: KindErr},
				// TODO(zeph): rest of array
			},
			err: "missing",
		},
		{
			name: "missing-last",
			src:  `null,]`,
			want: []Elem{
				{Content: "[", Neighbor: 0, Kind: KindErr},
				{Content: "null", Neighbor: 0, Kind: KindNull},
				{Content: ",", Neighbor: 0, Kind: KindComma},
				{Content: "]", Neighbor: 0, Kind: KindErr},
				{Content: "", Neighbor: 0, Kind: KindErr},
			},
			err: "opening",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			v, r, err := p.array(nil, Elem{Content: "[", Kind: KindListStart})
			// Append the terminating element to v to make comparison easy.
			v = append(v, r)
			if !slices.Equal(v, c.want) {
				t.Errorf("wrong result:\nwant %#v\ngot  %#v", c.want, v)
			}
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

func TestParseValue(t *testing.T) {
	cases := []struct {
		name string
		src  string
		want []Elem
		err  string
	}{
		{
			name: "empty",
			src:  ``,
			want: nil,
			err:  "EOF",
		},
		{
			name: "null",
			src:  `null`,
			want: []Elem{
				{Content: "null", Kind: KindNull},
			},
		},
		{
			name: "false",
			src:  `false`,
			want: []Elem{
				{Content: "false", Kind: KindFalse},
			},
		},
		{
			name: "true",
			src:  `true`,
			want: []Elem{
				{Content: "true", Kind: KindTrue},
			},
		},
		{
			name: "spacey",
			src:  `null `,
			want: []Elem{
				{Content: "null", Kind: KindNull, Spaces: 1},
			},
		},
		{
			name: "odd-spacey",
			src:  "null\r",
			want: []Elem{
				{Content: "null", Kind: KindNull},
				{Content: "\r", Kind: KindSpace},
			},
		},
		{
			name: "number",
			src:  `0`,
			want: []Elem{
				{Content: "0", Kind: KindNum},
			},
		},
		{
			name: "negative",
			src:  `-1`,
			want: []Elem{
				{Content: "-1", Kind: KindNum},
			},
		},
		{
			name: "string",
			src:  `""`,
			want: []Elem{
				{Content: `""`, Kind: KindString},
			},
		},
		{
			name: "array",
			src:  `["",0]`,
			want: []Elem{
				{Content: `[`, Kind: KindListStart, Neighbor: 4},
				{Content: `""`, Kind: KindString},
				{Content: `,`, Kind: KindComma},
				{Content: `0`, Kind: KindNum},
				{Content: `]`, Kind: KindListEnd, Neighbor: -4},
			},
		},
		{
			name: "fake-null",
			src:  `nul`,
			want: []Elem{
				// TODO(zeph): should not be empty
				// {Content: "nul", Kind: KindErr},
			},
			err: "literal",
		},
		{
			name: "fake-false",
			src:  `fals`,
			want: []Elem{
				// TODO(zeph): should not be empty
				// {Content: "fals", Kind: KindErr},
			},
			err: "literal",
		},
		{
			name: "fake-true",
			src:  `tru`,
			want: []Elem{
				// TODO(zeph): should not be empty
				// {Content: "tru", Kind: KindErr},
			},
			err: "literal",
		},
		{
			name: "illegal-char",
			src:  `$`,
			want: []Elem{
				{Content: "$", Kind: KindErr},
			},
			err: "syntax",
		},
		{
			name: "close-array",
			src:  `]`,
			want: []Elem{
				{Content: "]", Kind: KindErr},
			},
			err: "opening",
		},
		{
			name: "close-array-space",
			src:  `] `,
			want: []Elem{
				{Content: "]", Kind: KindErr, Spaces: 1},
			},
			err: "opening",
		},
		{
			name: "close-object",
			src:  `}`,
			want: []Elem{
				{Content: "}", Kind: KindErr},
			},
			err: "opening",
		},
		{
			name: "close-object-space",
			src:  `} `,
			want: []Elem{
				{Content: "}", Kind: KindErr, Spaces: 1},
			},
			err: "opening",
		},
	}
	for _, c := range cases {
		c := c
		t.Run(c.name, func(t *testing.T) {
			p := Parse(strings.NewReader(c.src))
			got, err := p.value(nil)
			if !slices.Equal(got, c.want) {
				t.Errorf("wrong result:\nwant %#v\ngot  %#v", c.want, got)
			}
			if err != nil {
				if c.err == "" {
					t.Errorf("unexpected error %v", err)
				} else if !strings.Contains(err.Error(), c.err) {
					t.Errorf("error message %q doesn't contain %q", err.Error(), c.err)
				}
			} else if c.err != "" {
				t.Errorf("expected error containing %q but got nil", c.err)
			}
		})
	}
}

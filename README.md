# jsontape

Package jsontape parses JSON into a concrete syntax tape for easy iteration and editing.

A syntax tape is similar to a syntax tree but encodes elements in a linear structure rather than a tree.
Structural relationships are described using offsets between related elements rather than formal inclusion with pointers.
This makes moving to neighbors of a given node trivial; where a tree requires knowledge of the full path to the node and might require backtracking arbitrarily far, a tape allows simply incrementing an index.
It also allows faster parsing of all but very large documents, since allocations are automatically chunked.

The syntax tape is concrete in the sense that it preserves grammatical terms in addition to content terms.
Abstract syntax represents elements like braces, commas, &c. implicitly by the presence of a node that requires them.
The concrete approach means that documents can be round-tripped precisely, even if it's invalid JSON.
